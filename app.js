
/**
 * Module dependencies.
 */

var express = require('express'),
	routes = require('./routes'),
	http = require('http'),
	path = require('path');

var app = express();


app.configure(function(){
	app.set('port', process.env.PORT || 1234);
	app.set('views', __dirname + '/views');
	app.set('view engine', 'jade');
	app.use(express.favicon());
	app.use(express.logger('dev'));
	app.use(express.bodyParser());
	app.use(express.methodOverride());
	app.use(express.cookieParser('785tvgh85thirtg589tnv857tgvb54t25986tgbv356t'));
	app.use(express.session());
	app.use(app.router);
	app.use(require('less-middleware')({ src: __dirname + '/public' }));
	app.use(express.static(path.join(__dirname, 'public')));
});

app.configure('development', function(){
	console.log("Development mode");
	app.set('view options', { debug: true })
	app.use(express.errorHandler());
});

// Add routes
app.get('/', routes.index);
app.get('/config', routes.config);

var server = http.createServer(app);
server.listen(app.get('port'), function(){
	console.log("Express server listening on port " + app.get('port'));
});

// Initialize Socket.IO
var io = require('socket.io').listen(server);
