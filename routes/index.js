
/*
 * GET home page.
 */

exports.index = function(req, res)
{
	try
	{
		res.render('index', { title: 'Home' });
	}
	catch(e)
	{
		console.log("Caught an error!");
		console.log(e);
	}
};

exports.config = function(req, res)
{
	try
	{
		res.render('index', { title: 'Configuration' });
	}
	catch(e)
	{
		console.log(e);
	}
};
